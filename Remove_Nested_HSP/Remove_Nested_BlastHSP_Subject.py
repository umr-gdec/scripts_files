#!/usr/bin/python3

import sys
from collections import defaultdict


good_HSP=defaultdict(dict)
cmptr=0
start_hit=[]
end_hit=[]
prct_hit=[]
len_hit=[]
nb_line=0
hit_done=[]
line_done=[]



def pop_lists(hit_done,id_hit,start_hit,end_hit,len_hit,prct_hit,strt,end,len_HSP,prct_HSP,good_HSP,ll):
	"""Delete element of each list"""

	hit_done.append(id_hit)
	start_hit.pop(-1)
	end_hit.pop(-1)
	len_hit.pop(-1)
	prct_hit.pop(-1)

	start_hit.append(int(strt))
	end_hit.append(int(end))

	len_hit.append(len_HSP)
	prct_hit.append(prct_HSP)						


	good_HSP[id_hit][strt,end,prct_HSP]=[ll]


	return(hit_done,start_hit,end_hit,len_hit,prct_hit,good_HSP)



with open(sys.argv[1],"r") as f1:
	for ligne in f1:
		cmptr+=1
		nb_line+=1

		li=ligne.rstrip().split()
		ll=ligne.rstrip()

		id_hit=str(nb_line)+"_"+str(li[8])+"_"+str(li[9])
		prct_HSP=float(li[2])
		len_HSP=int(li[3])

		line_done.append(ll)

		check=0

		if cmptr==1:
			start_hit.append(int(li[8]))
			end_hit.append(int(li[9]))

			len_hit.append(len_HSP)
			prct_hit.append(prct_HSP)

			good_HSP[id_hit][li[8],li[9],prct_HSP]=[ll]
			hit_done.append(id_hit)

		else:
			if int(li[8]) > int(end_hit[0]):#Two different hits
					if id_hit not in hit_done:

						pop_lists(hit_done,id_hit,start_hit,end_hit,len_hit,prct_hit,li[8],li[9],len_HSP,prct_HSP,good_HSP,ll)
			
			elif int(li[8]) >= int(start_hit[0]) and int(li[8]) <= int(end_hit[0]):#Overlap or repeat ?

				if int(li[9]) <= int(end_hit[0]): #Repeat
					if int(len_HSP) / int(len_hit[0]) > 0.95 and prct_hit[0] <= prct_HSP :
									

						if id_hit not in hit_done:
							del good_HSP[hit_done[-1]]

							pop_lists(hit_done,id_hit,start_hit,end_hit,len_hit,prct_hit,li[8],li[9],len_HSP,prct_HSP,good_HSP,ll)

 
				if int(li[9]) > int(end_hit[0]):#Overlap

					if int(li[8]) >= int(start_hit[0])+50 and int(li[9]) > int(end_hit[0])+50 or int(end_hit[0])-int(li[8]) <= 10  :#Real new start
						if id_hit not in hit_done:

							pop_lists(hit_done,id_hit,start_hit,end_hit,len_hit,prct_hit,li[8],li[9],len_HSP,prct_HSP,good_HSP,ll)


					elif int(len_HSP) / int(len_hit[0]) > 1.05 :#Bigger hit repeat
						if id_hit not in hit_done:
							del good_HSP[hit_done[-1]]

							pop_lists(hit_done,id_hit,start_hit,end_hit,len_hit,prct_hit,li[8],li[9],len_HSP,prct_HSP,good_HSP,ll)

					elif int(len_HSP) / int(len_hit[0]) > 0.95 and prct_hit[0] <= prct_HSP :#Repeat2
						
						if id_hit not in hit_done:

							del good_HSP[hit_done[-1]]

							pop_lists(hit_done,id_hit,start_hit,end_hit,len_hit,prct_hit,li[8],li[9],len_HSP,prct_HSP,good_HSP,ll)




					
							
					else:
						check+=1


for cle in good_HSP:
	for scle in good_HSP[cle]:
		print(good_HSP[cle][scle][0])
				
					
