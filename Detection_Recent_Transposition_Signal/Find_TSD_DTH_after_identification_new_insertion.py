#!/usr/bin/python3

import sys
import re
from collections import defaultdict

snp_value={}
snp_value["A"]=["T","C","G"]
snp_value["T"]=["A","C","G"]
snp_value["C"]=["T","A","G"]
snp_value["G"]=["T","C","A"]
snp_value["N"]=["T","C","G","A"]

reverse_dict={}
reverse_dict["A"]="T"
reverse_dict["C"]="G"
reverse_dict["G"]="C"
reverse_dict["T"]="A"
reverse_dict["N"]="N"

tested_tsd_snp=defaultdict(dict)
perfect_match={}
tir_snp_match={}
tsd_snp_match={}
good_result=[]
waiting=[]
with open(sys.argv[1],"r") as f0:
	for ligne in f0:
		li=ligne.rstrip().split()
		ll=ligne.rstrip()
		fiveprime=str(li[3].upper())
		threeprime=str(li[1].upper())

		tsd_fivep=[]
		tsd_threep=[]
		TIR_5p=[]
		TIR_3p=[]
		reverse_5p=[]

		end_tsdfivep=[]
		start_tsdthreep=[]
		start_comp=[]
		end_comp=[]

		for i in range(0,len(fiveprime)-8):
			tsd_fivep.append(fiveprime[i:i+3])
			end_tsdfivep.append(i)
			TIR_5p.append(fiveprime[i+3:i+8])
			tir=fiveprime[i+3:i+8]
			reverse_TIR5p=""
			for nucl in tir:
				if nucl in reverse_dict:
					reverse_TIR5p=reverse_TIR5p+reverse_dict[nucl]

			reverse_5p.append(reverse_TIR5p[::-1])
			
			
		for i in range(5,len(threeprime)-3):
			tsd_threep.append(threeprime[i:i+3])
			start_tsdthreep.append(i)
			TIR_3p.append(threeprime[i-5:i])
			#print(li, TIR_3p,reverse_5p)

		if len(tsd_fivep) > 0 and len (tsd_threep) > 0 :
			for tir in TIR_3p:
				if tir in reverse_5p:
					
					index1=TIR_3p.index(tir)
					tsd_3p=tsd_threep[index1]
				
					index2=reverse_5p.index(tir)
					tsd_5p=tsd_fivep[index2]

					if tsd_5p == tsd_3p:
						perfect_match[li[0]]=[ll,tir, tsd_3p, tsd_5p]
						#print(ll, tir, tsd_3p, tsd_5p)	
						good_result.append(ll)
				
						if li[0] in tsd_snp_match:
							del tsd_snp_match[li[0]]
					
					else:
						if li[0] not in perfect_match:
							for pos in range(0,len(tsd_3p)):
								for snp in snp_value[tsd_3p[pos]]:
									tsd_snp=tsd_3p[0:pos]+snp+tsd_3p[pos+1:len(tsd_3p)]

									if tsd_snp == tsd_5p:
										tsd_snp_match[li[0]]=[ll,tir,tsd_3p, tsd_5p]



				if tir not in reverse_5p:
					for tsd in tsd_fivep:
						indice1=[i for i, x in enumerate(tsd_fivep) if x == tsd]
						
						if tsd in tsd_threep:
							indice2=[j for j, y in enumerate(tsd_threep) if y == tsd]
							
							if len(indice1) > 1 or len(indice2) > 1:
								for indi in indice1:
									for ind in indice2:
										rev_TIR5p=reverse_5p[indi]
										TIR3p=TIR_3p[ind]
								
										for pos in range(0,len(TIR3p)):
											for snp in snp_value[TIR3p[pos]]:
                                                                                		TIR3p_snp=TIR3p[0:pos]+snp+TIR3p[pos+1:len(TIR3p)]

                                                                                		if TIR3p_snp == rev_TIR5p:
                                                                                        		tir_snp_match[li[0]]=[ll, tsd, rev_TIR5p, TIR3p, TIR3p_snp]


							else:
								index11=tsd_fivep.index(tsd)
								rev_TIR5p=reverse_5p[index11]

								index22=tsd_threep.index(tsd)
								TIR3p=TIR_3p[index22]
							
								for pos in range(0,len(TIR3p)):
									for snp in snp_value[TIR3p[pos]]:
										TIR3p_snp=TIR3p[0:pos]+snp+TIR3p[pos+1:len(TIR3p)]

										if TIR3p_snp == rev_TIR5p:
											tir_snp_match[li[0]]=[ll, tsd, rev_TIR5p, TIR3p, TIR3p_snp]	
					
						



for cle in perfect_match:
	if cle in tsd_snp_match:
		del tsd_snp_match[cle]

	if cle in tir_snp_match:
		del tir_snp_match[cle]
	print(*perfect_match[cle], sep=" ")
	#print("Perfect",*perfect_match[cle], sep=" ")

for cle in tir_snp_match:
	if cle in tsd_snp_match:
		del tsd_snp_match[cle]

	#print("SNP_TIR",*tir_snp_match[cle], sep=" ")
	print(*tir_snp_match[cle], sep=" ")

for cle in tsd_snp_match:
        print(*tsd_snp_match[cle], sep=" ")
	#print("SNP_tsd",*tsd_snp_match[cle], sep=" ")
