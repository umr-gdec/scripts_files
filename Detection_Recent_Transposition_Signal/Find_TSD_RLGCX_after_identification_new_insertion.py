#!/usr/bin/python3

import sys
import re
from collections import defaultdict

snp_value={}
snp_value["A"]=["T","C","G"]
snp_value["T"]=["A","C","G"]
snp_value["C"]=["T","A","G"]
snp_value["G"]=["T","C","A"]
snp_value["N"]=["T","C","G","A"]



tested_tsd_snp=defaultdict(dict)
perfect_match={}
snp_match={}

with open(sys.argv[1],"r") as f0:
	for ligne in f0:
		li=ligne.rstrip().split()
		ll=ligne.rstrip()
		fiveprime=str(li[3].upper())
		threeprime=str(li[1].upper())

		tsd_fivep=[]
		tsd_threep=[]

		end_tsdfivep=[]
		start_tsdthreep=[]
		start_comp=[]
		end_comp=[]

		for i in range(0,len(fiveprime)-1):
			if i >=5 and  fiveprime[i] == "T" and fiveprime[i+1] == "G":
				tsd_fivep.append(fiveprime[i-5:i])
				end_tsdfivep.append(i-1)
				#print(li[2],fiveprime,fiveprime[i-5:i+2])
			
		for i in range(0,len(threeprime)-1):
			if i <= 18 and  threeprime[i] == "C" and threeprime[i+1] == "A":
				tsd_threep.append(threeprime[i+2:i+7])
				start_tsdthreep.append(i+2)
                                #print(li[0],threeprime,threeprime[i:i+7])

		if len(tsd_fivep) > 0 and len (tsd_threep) > 0 :
			for tsd in tsd_fivep:
				if tsd in tsd_threep:
					index1=tsd_threep.index(tsd)
					start=start_tsdthreep[index1]
				
					index2=tsd_fivep.index(tsd)
					end=end_tsdfivep[index2]
				
					if ll not in perfect_match :
						perfect_match[ll]=[tsd,end,start]
					if ll in snp_match:
						del snp_match[ll]
				

			
				elif tsd not in tsd_threep:
					for pos in range(0,len(tsd)):
						for snp in snp_value[tsd[pos]]:
							tsd_snp=tsd[0:pos]+snp+tsd[pos+1:len(tsd)]
						
							if tsd_snp in tsd_threep:
							
								index1=tsd_threep.index(tsd_snp)
								start=start_tsdthreep[index1]
								index2=tsd_fivep.index(tsd)
								end=end_tsdfivep[index2]
							
								if ll not in perfect_match:
									if ll not in snp_match:
										snp_match[ll]=[tsd,end,start,tsd_snp]					

for cle in perfect_match:
	if cle in snp_match:
		del snp[cle]

for cle in perfect_match:
	print(cle,*perfect_match[cle],sep=" ")

for cle in snp_match:
	print(cle,*snp_match[cle],sep=" ")

#print(len(perfect_match))
#print(len(snp_match))

