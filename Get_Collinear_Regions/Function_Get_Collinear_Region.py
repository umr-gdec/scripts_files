#!/usr/bin/python3
from collections import defaultdict
import sys


def category_cds(CDS_file):
	"""Cette fonction permet de répartir les différents paires de CDS dans divers dictionnaires en fonction de leur position respectives potentiellement problématique pour la détection de la syntenie (1 ou plusieurs CDS nichés dans un autre...)"""
	cmptr=0
	sort_cds={}
	pos_cds={}
	brin_cds={}

	lst1=[]
	lst2=[]
	cds_gene=[]

	new_chrom=[]


	stop_container={}
	container={}
	nested={}

	#Récupération des informations des genes a partir du fichier GFF3
	with open(CDS_file, "r") as f2:
		for ligne in f2:
			if ligne.startswith("chr"):
				lig=ligne.rstrip().split()
				if lig[2] == "gene":
					cmptr+=1

					if lig[0] not in new_chrom:
						new_chrom.append(lig[0])
						lst1=[]
						lst2=[]
						cds_gene=[]

					if lig[0] == "chrUn":
						id_cds=lig[8][:20]
				
					else:
						id_cds=lig[8][:21]
					
					sort_cds[id_cds]=cmptr
					pos_cds[id_cds]=[lig[3], lig[4]]
					brin_cds[id_cds]=lig[6]

					lst1.append(lig[3])
					lst2.append(lig[4])
					cds_gene.append(id_cds)

					if len(lst1) > 1 :
						start1=int(lst1[0])
						start2=int(lst1[1])

						end1=int(lst2[0])
						end2=int(lst2[1])


						if start1 <= start2 and end2 <= end1:
							if cds_gene[0] not in container:
								container[cds_gene[0]]=[cds_gene[1]]
								nested[cds_gene[1]]=[cds_gene[0]]
		
							else:
								container[cds_gene[0]].append(cds_gene[1])
								nested[cds_gene[1]]=[cds_gene[0]]  
								
							lst1.pop(1)
							lst2.pop(1)
							cds_gene.pop(1)	

						else:
						
							if cds_gene[0] in container:
								if cds_gene[1] not in stop_container:
									stop_container[cds_gene[1]]=[cds_gene[0]]
						
								else:
									stop_container[cds_gene[1]].append(cds_gene[0])

							lst1.pop(0)
							lst2.pop(0)
							cds_gene.pop(0)

	before_container={}
	tmp_beglist=[]
	tmp_neslist=[]
	for cle in container:
		cont=sort_cds[cle]
		bef_cont=cont-1
		nest=cont+1
		for clef,valeur in sort_cds.items():
			if bef_cont == valeur:
				tmp_beglist.append(clef)

			if nest == valeur:
				tmp_neslist.append(clef)

	
			if len(tmp_beglist) == 1 and len(tmp_neslist) == 1:
				if tmp_beglist[0] not in before_container:
					before_container[tmp_beglist[0]]=[tmp_neslist[0]]

				tmp_beglist=[]
				tmp_neslist=[]


	list_dic=[sort_cds,pos_cds,brin_cds,container,nested,stop_container,before_container]
	return list_dic



def find_duplicate(file_gmap,sort_cds):
	"""Identify duplicate genes in subject (Chinese_Spring) not present in query to ease the collinear analysis """

	id_gene=[]
	strt_gene=[]
	end_gene=[]
	current_dupli_beg=defaultdict(dict)
	current_dupli_end=defaultdict(dict)
	duplicate=[]
	all_duplicate=[]
	cmptr=0
	with open(file_gmap,"r") as f1:
		for ligne in f1:
			li = ligne.rstrip().split()
			ll = ligne.rstrip()
		
			id_gene.append(li[3])
			strt_gene.append(li[1])
			end_gene.append(li[2])
			

			if len(id_gene) == 3 :#Detection CDS with similar start or stop
				if len(strt_gene) == len(set(strt_gene)) and len(end_gene) == len(set(end_gene)):#No duplication
			
					id_gene.pop(0) 
					strt_gene.pop(0)
					end_gene.pop(0) 
				

				elif len(set(strt_gene)) == 1 or len(set(end_gene)) == 1: #Duplication continue
					if len(current_dupli_beg) == 0 :#Prise en compte où le fichier d'alignement commencerait par une duplication
						
						beg_dup=id_gene[0]
						
						current_dupli_beg[cmptr][id_gene[0]]=[]
						for elem in id_gene[1:len(id_gene)]:
							if elem not in duplicate:
								duplicate.append(elem)
								current_dupli_beg[cmptr][beg_dup].append(elem)

							if elem not in all_duplicate:
								all_duplicate.append(elem)
					
						id_gene.pop(0)
						strt_gene.pop(0)
						end_gene.pop(0)

					else:
						for element in id_gene:
							#print(element)
							for sskey in current_dupli_beg[cmptr]:
								if element != sskey:
									if element not in current_dupli_beg[cmptr][sskey]: 
										current_dupli_beg[cmptr][sskey].append(element)
						id_gene.pop(0)
						strt_gene.pop(0)
						end_gene.pop(0)


				elif len(set(strt_gene)) > 1 or len(set(end_gene)) > 1:#Duplication same start/stop
					if strt_gene[0] == strt_gene[1] or end_gene[0] == end_gene[1]:#Fin de la duplication
						if len(current_dupli_beg) > 0 :

							current_dupli_end[cmptr][id_gene[2]]=current_dupli_beg[cmptr][beg_dup]
						

						for elem in id_gene:
							if elem not in all_duplicate:
								all_duplicate.append(elem)	

					if strt_gene[1] == strt_gene[2] or end_gene[1] == end_gene[2]:#Début de la duplication
						

						cmptr+=1
						all_duplicate.append(id_gene[1:3])

						beg_dup=id_gene[0]

				
						if beg_dup not in duplicate:
							current_dupli_beg[cmptr][beg_dup]=[id_gene[1], id_gene[2]]


						else:
							valeur=list(current_dupli_end[cmptr-1].values())[0]
							current_dupli_end[cmptr-1][id_gene[2]]=valeur #Si End est une duplication conservation de l'information (end duplication pourra être l'un ou l'autre des CDS)
							current_dupli_end[cmptr-1][id_gene[1]]=valeur

							for elem in duplicate:
								current_dupli_beg[cmptr][elem]=[id_gene[1], id_gene[2]]
						duplicate=[]

						duplicate.append(id_gene[1])
						duplicate.append(id_gene[2])

					id_gene.pop(0)
					strt_gene.pop(0)
					end_gene.pop(0)	
	list_toappend=[]
	for cle in current_dupli_beg:
		for sskey in current_dupli_beg[cle]:
			for elm in current_dupli_beg[cle][sskey]:
				for key in current_dupli_beg:
					for subkey in current_dupli_beg[key]:
						if elm == subkey:
							list_toappend.append([elm,[current_dupli_beg[cle][sskey]],current_dupli_beg[key][subkey],key])
	#print(list_toappend)

	for cds in list_toappend:
		for dupl in cds[1][0]:
			#print(dupl)
			if dupl != cds[0]:
				current_dupli_beg[cds[3]][dupl]=cds[2]


	id_dup={}
	tandem_dupli={}
	pair_toavoid={}
	for key in current_dupli_beg:
		for sskey in current_dupli_beg[key]:
			
			current_pair=[]
			current_score=[]
			synteny_score=0
			tmp_score_synt_beg=[]
			tmp_pair_synt_beg=[]
			for i in current_dupli_beg[key][sskey]:
				if abs(int(sort_cds[i]) - int(sort_cds[sskey])) == 1 :
					tmp_score_synt_beg.append(int(sort_cds[i]) - int(sort_cds[sskey]))
					tmp_pair_synt_beg.append([sskey,i])

					if i in current_dupli_end[key-1].keys() :
						synteny_score=int(sort_cds[i]) - int(sort_cds[sskey])
						current_score.append(synteny_score)
						current_pair.append([sskey,i])
					
					
					
					else:
						if sskey not in id_dup:
							id_dup[sskey]=[[sskey,i]]
						else:
							if [sskey,i] not in id_dup[sskey]:				
								id_dup[sskey].append([sskey,i])

				if len(current_score) > 1 :			
					max_val=max(current_score)		
					index=current_score.index(max_val)
				
					tandem_dupli[current_pair[index][0]]=[current_pair[index]]
					current_pair.pop(index)
					current_score.pop(index)

					pair_toavoid[current_pair[0][0]]=current_pair[0][1]
					pair_toavoid[current_pair[0][1]]=current_pair[0][0]

			if len(tmp_pair_synt_beg) == 1:
				if tmp_pair_synt_beg[0][0] not in id_dup:
					id_dup[tmp_pair_synt_beg[0][0]]=[[tmp_pair_synt_beg[0][0],tmp_pair_synt_beg[0][1]]]

				else:
					if [tmp_pair_synt_beg[0][0],tmp_pair_synt_beg[0][1]] not in id_dup[tmp_pair_synt_beg[0][0]]:
						id_dup[tmp_pair_synt_beg[0][0]].append([tmp_pair_synt_beg[0][0],tmp_pair_synt_beg[0][1]])

			if len(tmp_pair_synt_beg) > 1 :
			
				index_beg=tmp_score_synt_beg.index(1)
				avoid_beg=tmp_score_synt_beg.index(-1)


				if tmp_pair_synt_beg[index_beg][0] not in id_dup:
					id_dup[tmp_pair_synt_beg[index_beg][0]]=[[tmp_pair_synt_beg[index_beg][0],tmp_pair_synt_beg[index_beg][1]]]

				else:
					if [tmp_pair_synt_beg[index_beg][0],tmp_pair_synt_beg[index_beg][1]] not in id_dup[tmp_pair_synt_beg[index_beg][0]]:
						id_dup[tmp_pair_synt_beg[index_beg][0]].append([tmp_pair_synt_beg[index_beg][0],tmp_pair_synt_beg[index_beg][1]])
				pair_toavoid[tmp_pair_synt_beg[avoid_beg][0]]=[tmp_pair_synt_beg[avoid_beg][1]]
				pair_toavoid[tmp_pair_synt_beg[avoid_beg][1]]=[tmp_pair_synt_beg[avoid_beg][0]]

	for key in current_dupli_end:
		for sskey in current_dupli_end[key]:
			tmp_score_synt=[]
			tmp_pair_synt=[]
			for i in current_dupli_end[key][sskey]:
				if abs(int(sort_cds[i]) - int(sort_cds[sskey])) == 1 :
					tmp_score_synt.append(int(sort_cds[i]) - int(sort_cds[sskey]))
					tmp_pair_synt.append([i, sskey])

			if len(tmp_pair_synt) == 1:
				if tmp_pair_synt[0][0] not in id_dup:
					id_dup[tmp_pair_synt[0][0]]=[[tmp_pair_synt[0][0],tmp_pair_synt[0][1]]]

				else:
					if [tmp_pair_synt[0][0],tmp_pair_synt[0][1]] not in id_dup[tmp_pair_synt[0][0]]:
						id_dup[tmp_pair_synt[0][0]].append([tmp_pair_synt[0][0],tmp_pair_synt[0][1]])

			if len(tmp_pair_synt) > 1 :

				index=tmp_score_synt.index(-1)
				avoid=tmp_score_synt.index(1)

				if tmp_pair_synt[index][0] not in id_dup:
					id_dup[tmp_pair_synt[index][0]]=[[tmp_pair_synt[index][0],tmp_pair_synt[index][1]]]

				else:
					if [tmp_pair_synt[index][0],tmp_pair_synt[index][1]] not in id_dup[tmp_pair_synt[index][0]]:
						id_dup[tmp_pair_synt[index][0]].append([tmp_pair_synt[index][0],tmp_pair_synt[index][1]])

				pair_toavoid[tmp_pair_synt[avoid][0]]=[tmp_pair_synt[avoid][1]]
				pair_toavoid[tmp_pair_synt[avoid][1]]=[tmp_pair_synt[avoid][0]]

	for cle in tandem_dupli:
		if cle in id_dup:
			del id_dup[cle]
			id_dup[cle]=tandem_dupli[cle]

	info_dup=[id_dup,all_duplicate,pair_toavoid]
	return info_dup


def output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,chrom,id_entier,num_cds):
	"""Gather all informations for the outpurt file"""
	brin_final_Cs=list(set(brin_Cs))

	if len(brin_final_Cs) == 2:
		strand_Cs=brin_final_Cs[0]+"/"+brin_final_Cs[1]
	else:
		strand_Cs=brin_final_Cs[0]
					
	brin_final_Zav=list(set(brin_Zav))

	if len(brin_final_Zav) == 2:
		strand_Zav=brin_final_Zav[0]+"/"+brin_final_Zav[1]
	else:
		strand_Zav=brin_final_Zav[0]


	if int(seq_zav_end[-1]) > int(seq_zav_beg[0]) and int(seq_cs_beg[0]) <= int(seq_cs_beg[-1]) and int(seq_cs_end[0]) <= int(seq_cs_end[-1]):
		print(chrom, int(seq_zav_beg[0]), int(seq_zav_end[-1]), strand_Zav, id_entier[0], id_entier[-1], seq_cs_beg[0], seq_cs_end[-1], strand_Cs, len(id_entier)) #cds in expected order

	elif int(seq_zav_end[-1]) > int(seq_zav_beg[0]) and int(seq_cs_beg[0]) <= int(seq_cs_beg[-1]) and int(seq_cs_end[0]) >= int(seq_cs_end[-1]):
		print(chrom, int(seq_zav_beg[0]), int(seq_zav_end[-1]), strand_Zav, id_entier[0], id_entier[-1], seq_cs_beg[0], seq_cs_end[-1], strand_Cs, len(id_entier)) # cds1 nested in cds0

	elif int(seq_zav_end[-1]) > int(seq_zav_beg[0]) and int(seq_cs_beg[0]) >= int(seq_cs_beg[-1]) and int(seq_cs_end[0]) >= int(seq_cs_end[-1]):
		print(chrom, int(seq_zav_beg[0]), int(seq_zav_end[-1]), strand_Zav, id_entier[0], id_entier[-1], seq_cs_beg[-1], seq_cs_end[0], strand_Cs, len(id_entier)) #cds in inverted order

	elif int(seq_zav_end[-1]) > int(seq_zav_beg[0]) and int(seq_cs_beg[0]) >= int(seq_cs_beg[-1]) and int(seq_cs_end[0]) <= int(seq_cs_end[-1]):
		print(chrom, int(seq_zav_beg[0]), int(seq_zav_end[-1]), strand_Zav, id_entier[0], id_entier[-1], seq_cs_beg[0], seq_cs_end[1], strand_Cs, len(id_entier)) #cds0 nested in cds1
 

	else:
		if int(seq_cs_beg[0]) > int(seq_cs_beg[1]) and int(seq_cs_end[0]) > int(seq_cs_end[1]) and int(seq_zav_end[-1]) > int(seq_zav_beg[0]):
			print(chrom, int(seq_zav_beg[0]), int(seq_zav_end[1]), strand_Zav, id_entier[0], id_entier[-1], int(seq_cs_beg[1]), int(seq_cs_end[0]), strand_Cs, len(id_entier))
		

		else:
			sys.exit("An error occured during the process (wrong localisation)")



def pop_liste(id_entier,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,num_cds,brin_Cs,brin_Zav):
	"""Delete element of each list"""
				
	id_entier.pop(0)
	seq_zav_beg.pop(0)
	seq_zav_end.pop(0)
	seq_cs_beg.pop(0)
	seq_cs_end.pop(0)
	num_cds.pop(0)
	brin_Cs.pop(0)
	brin_Zav.pop(0)

	return(id_entier,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,num_cds,brin_Cs,brin_Zav)
