#!/usr/bin/python3

import sys
from Function_Get_Collinear_Region  import *
#from collections import defaultdict

try:
	sys.argv[2]
except IndexError:
	print("Two files needed for the analysis")
	print("1°) GMAP-type output file (BED format)")
	print("2°) GFF3 file containing Chinese spring CDS")
	
	sys.exit()

CDS_file=sys.argv[2]



list_dico=category_cds(CDS_file)

sort_cds=list_dico[0]
pos_cds=list_dico[1]
brin_cds=list_dico[2]
container=list_dico[3]
nested=list_dico[4]
stop_container=list_dico[5]
before_container=list_dico[6]

'''
align={}
nb_line=0
with open(sys.argv[1],"r") as f0:
	for ligne in f0:
		nb_line+=1
		li = ligne.rstrip().split()
		if li[3] in sort_cds:
			align[sort_cds[li[3]]]=li[3]

'''

info_dup=find_duplicate(sys.argv[1],sort_cds)
all_duplicate=info_dup[1]
id_dup=info_dup[0]
pair_toavoid=info_dup[2]

liste_id_dup=[]
for element in id_dup.values():
	for elem in element:
		for el in elem:
			if el not in liste_id_dup:
				liste_id_dup.append(el)



comptr=0
seq_zav_beg=[]
seq_zav_end=[]

seq_cs_beg=[]
seq_cs_end=[]

id_entier=[]
num_cds=[]
brin_Cs=[]
brin_Zav=[]

chromc={}
lst_chr=[]
analysed_container=[]

duplication={}
info_dupli={}
#Travail sur le fichier d'alignement 
with open(sys.argv[1],"r") as f1:
	for ligne in f1:
		li = ligne.rstrip().split()
		comptr+=1
		id_gene = li[3]
		strand_Zav = li[4]			
		chrom = li[0]
		
		chrom_Cs=li[3][10:12]

			
		if chrom not in chromc:#Réinitialisation des listes après changement de chromosome
			chromc[chrom]=1			
			lst_chr.append(chrom)
			
			if len(id_entier) > 1 : #Si la détection d'un bloc de synténie était en cours, "print" les gènes synténiques				
				output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,lst_chr[-2],id_entier,num_cds)

			seq_zav_beg=[]
			seq_zav_end=[]

			seq_cs_beg=[]
			seq_cs_end=[]

			id_entier=[]
			num_cds=[]

			brin_Cs=[]
			brin_Zav=[]


		if li[3] in sort_cds:

			id_entier.append(li[3])#ID CDS
			
			seq_zav_beg.append(li[1]) #Begin
			seq_zav_end.append(li[2]) #End

			seq_cs_beg.append(pos_cds[li[3]][0]) #Begin
			seq_cs_end.append(pos_cds[li[3]][1]) #End
			
			num_cds.append(sort_cds[li[3]]) #Numero du CDS
			brin_Cs.append(brin_cds[li[3]]) #Brin_Cs
			brin_Zav.append(li[4]) #Brin_Zav		


		if len(id_entier) > 1 :

			if abs(int(num_cds[0]) - int(num_cds[1])) > 1 : #Cas où les gènes ne sont pas en synténie
				if id_entier[0] in nested and id_entier[1] in before_container:
					output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,chrom,id_entier,num_cds)
		
					del id_entier[0:len(id_entier)-1]
					del seq_zav_beg[0:len(seq_zav_beg)-1]
					del seq_zav_end[0:len(seq_zav_end)-1]
					del seq_cs_beg[0:len(seq_cs_beg)-1]
					del seq_cs_end[0:len(seq_cs_end)-1]
					del num_cds[0:len(num_cds)-1]
					del brin_Cs[0:len(brin_Cs)-1]
					del brin_Zav[0:len(brin_Zav)-1]

				if len(id_entier) > 1 :
					if id_entier[1] in container and id_entier[0] in stop_container and stop_container[id_entier[0]][0] == id_entier[1]:
						output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,chrom,id_entier,num_cds)
				

						del id_entier[0:len(id_entier)-1]
						del seq_zav_beg[0:len(seq_zav_beg)-1]
						del seq_zav_end[0:len(seq_zav_end)-1]
						del seq_cs_beg[0:len(seq_cs_beg)-1]
						del seq_cs_end[0:len(seq_cs_end)-1]
						del num_cds[0:len(num_cds)-1]
						del brin_Cs[0:len(brin_Cs)-1]
						del brin_Zav[0:len(brin_Zav)-1]


				pop_liste(id_entier,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,num_cds,brin_Cs,brin_Zav)


				

			elif abs(int(num_cds[-1])-int(num_cds[-2])) == 1 : #Cas où les gènes sont en synténie
				if seq_zav_beg[-1] == seq_zav_beg[-2] or seq_zav_end[-1] == seq_zav_end[-2]:# and seq_cs_beg[-1] != seq_cs_beg[-2] and seq_cs_end[-1] != seq_cs_end[-2] : #Détection des duplications géniques chez Chinese qui ne sont pas présentes chez l'autre espèce
				
					duplication[id_entier[-2], id_entier[-1]]=[chrom,seq_zav_beg[-2],seq_zav_end[-1]]


					#if id_entier[-2] in all_duplicate:
				

					pop_liste(id_entier,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,num_cds,brin_Cs,brin_Zav)


				if len(id_entier) > 1:
					if id_entier[0] in pair_toavoid and id_entier[1] in pair_toavoid[id_entier[0]]:
				
						pop_liste(id_entier,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,num_cds,brin_Cs,brin_Zav)


				if len(num_cds) == 2 :
					
					
					id_gene1=id_entier[0]
					id_gene2=id_entier[1]

					begin_zav2=int(seq_zav_beg[1])
					end_zav2=int(seq_zav_end[1])
					begin_zav1=int(seq_zav_beg[0])
					end_zav1=int(seq_zav_end[0])

					begin_cs2=int(seq_cs_beg[1])
					end_cs2=int(seq_cs_end[1])
					begin_cs1=int(seq_cs_beg[0])
					end_cs1=int(seq_cs_end[0])

					id_entier1=id_entier[0]
					id_entier2=id_entier[1]


					output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,chrom,id_entier,num_cds)

					
					del id_entier[0:len(id_entier)-1]
					del seq_zav_beg[0:len(seq_zav_beg)-1]
					del seq_zav_end[0:len(seq_zav_end)-1]
					del seq_cs_beg[0:len(seq_cs_beg)-1]
					del seq_cs_end[0:len(seq_cs_end)-1]
					del num_cds[0:len(num_cds)-1]
					del brin_Cs[0:len(brin_Cs)-1]
					del brin_Zav[0:len(brin_Zav)-1]

			elif abs(int(num_cds[-1])-int(num_cds[-2])) == 1 and int(comptr) == int(nb_line) : #Si un bloc de synténie est détecté en fin de fichier, "print" le bloc
				#print("End_file",id_entier, num_cds)
				output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,chrom,id_entier,num_cds)



		if li[3] in liste_id_dup:
			if li[3] not in info_dupli:
				info_dupli[li[3]]=[brin_cds[li[3]], li[4], li[1], li[2], pos_cds[li[3]][0], pos_cds[li[3]][1], chrom, sort_cds[li[3]]]


for cle in id_dup:
	for sslist in id_dup[cle]:
		seq_zav_beg=[]
		seq_zav_end=[]

		seq_cs_beg=[]
		seq_cs_end=[]

		id_entier=[]
		brin_Cs=[]
		brin_Zav=[]
		num_cds=[]

		for i in range(0,len(sslist)):
			
			id_entier.append(sslist[i])#ID CDS
			num_cds.append(info_dupli[sslist[i]][7])		

			seq_zav_beg.append(info_dupli[sslist[i]][2]) #Begin
			seq_zav_end.append(info_dupli[sslist[i]][3]) #End

			seq_cs_beg.append(info_dupli[sslist[i]][4]) #Begin
			seq_cs_end.append(info_dupli[sslist[i]][5]) #End
		
			brin_Cs.append(info_dupli[sslist[i]][0]) #Brin_Cs
			brin_Zav.append(info_dupli[sslist[i]][1]) #Brin_Zav

			chrom=info_dupli[sslist[i]][6]+"_dup"

			if len(id_entier) > 1:
				if id_entier[0] in pair_toavoid and id_entier[1] in pair_toavoid[id_entier[0]]:

					pop_liste(id_entier,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,num_cds,brin_Cs,brin_Zav)

			if len(id_entier) > 1:
				output_fmt(brin_Cs,brin_Zav,seq_zav_beg,seq_zav_end,seq_cs_beg,seq_cs_end,chrom,id_entier,num_cds)

