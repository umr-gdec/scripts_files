#!/usr/bin/env perl

###########################
## Included modules
###########################
##  Basic Perl modules
use strict;
use warnings;
use diagnostics;

## BioPerl modules
use Bio::Tools::GFF;

## Other Perl modules
use Getopt::Long;
use File::Basename;
use Switch;

## Debug
use Data::Dumper;
use Log::Log4perl qw(get_logger :levels);

##################################################
## Globals variables
##################################################

## Date and Version
my $version= '1.0';
my $version_date = '2016-09-04';
my $script_name = 'parseGmap.pl';

# Initializations
# sort keys when dump a hash
$Data::Dumper::Sortkeys = 1;
my $help = undef();
my %Parameters;

###########################
## Manage options & logger
###########################

# Get options from command line
GetOptions ('help|h'		=> \$help,
			'output|o=s'	=> \$Parameters{'output'},
			'gmap|g=s'		=> \$Parameters{'input'},
			'coverage|c=s'	=> \$Parameters{'coverage'},
			'identity|i=s'	=> \$Parameters{'identity'},
			'report|r=s'	=> \$Parameters{'report'},
			'debug|D=s'		=> \$Parameters{'log'},
		);

# set logger status
my $DEBUG = defined $Parameters{'log'} ? $Parameters{'log'} : 'INFO';
my $multiLogger = createLogger($DEBUG, 'yes');

&main();
sub main {
	##########################
	# INTRODUCTION
	##########################
	my $self={};
	bless $self;
	$self->setOptions();

	# Display help message if needed
	if ($self->{parameters}->{help}) {
		$self->displayHelpMessage();
	}

	# Check parameters
	$self->checkParameters();
	#$multiLogger->debug(Dumper ($self->{parameters}) . "\n");

	# Welcome message
	$multiLogger->info( " ###################################################################\n");
	$multiLogger->info( " #      Welcome in $script_name (Version $version)       #\n");
	$multiLogger->info( " ###################################################################\n");
	$multiLogger->debug( " # DUMPER Main: \n ", Dumper($self), "\n");

	# set output gff obj
	$self->{gffwriter} = Bio::Tools::GFF->new(	-file => ">$self->{files}->{output}",
												-gff_version => 3) ;

	# set various counters
	$self->initializeVar();

	# initial report
	$self->reportInitial();

	# load GFF from GMAP
	$self->readGFF();

	# report
	$self->reportFinal();
}

sub initializeVar {
	my $self = shift;

	$self->{totalWrittenFeatures}->{gene} = 0;
	$self->{totalWrittenFeatures}->{mrna} = 0;
	$self->{totalWrittenFeatures}->{exon} = 0;
	$self->{totalWrittenFeatures}->{cds} = 0;

}

sub reportInitial {
	my $self = shift;

	$multiLogger->info(' Parameters summary: ');
	$multiLogger->info(' Input GFF GMAP file selected: ', $self->{files}->{input});
	$multiLogger->info(' Output GFF file: ', $self->{files}->{output});
	$multiLogger->info(' min coverage percentage threshold: ', $self->{parameters}->{coverage});
	$multiLogger->info(' min identity percentage threshold: ', $self->{parameters}->{identity});
	$multiLogger->info(' which gmap results to report: ', $self->{parameters}->{report});
}

sub readGFF {
	my $self = shift;

	# create gff obj
	$self->{gffObj}= Bio::Tools::GFF->new(	-file => $self->{files}->{input},
												-gff_version => 3) ;

	while( $self->{gffFeature} = $self->{gffObj}->next_feature() ){
		#$multiLogger->debug(' contenu de $self: ', Dumper $self);

		# first check if the current feature is a gene and save it
		$self->isGeneFeature();

		# then test the mRAN feature for coverage and identity and path number
		$self->parseMrnaMappingStats();

		# finally, check for exons and cds features and print it if validHitFound == 1
		$self->addExonsCDSfromValidHits();

		#<STDIN>;
	}
}

sub isGeneFeature {
	my $self = shift;


	# save gene feature track for later use
	if ($self->{gffFeature}->{_primary_tag} eq 'gene'){
		$self->{currentGeneFeature} = $self->{gffFeature};
		# initialize validHitFound variable for new gene hit to check
		$self->{validHitFound} =0;

		#$multiLogger->debug(' current feature is a gene! saving into memory for later use: ', Dumper $self->{currentGeneFeature}, Dumper $self->{validHitFound});

	}
}

sub parseMrnaMappingStats {
	my $self = shift;

	if ($self->{gffFeature}->{_primary_tag} eq 'mRNA'){

		# replace . by , as decimal delimiter
		my $cov_tmp = $self->{gffFeature}->{_gsf_tag_hash}->{coverage}->[0];
		my $id_tmp = $self->{gffFeature}->{_gsf_tag_hash}->{identity}->[0];

		#$multiLogger->debug(" coverage= $cov_tmp and identity=$id_tmp to test.");

		# check for coverage and identity threshold
		if ( $cov_tmp >= $self->{parameters}->{coverage}
			&& $id_tmp >= $self->{parameters}->{identity}){
		#	$multiLogger->debug(' ************************** hit with cov and id over thresholds: ', Dumper $self->{gffFeature}->{_gsf_tag_hash});


			# always print path1 hit if thresholds are ok
			my $parent = $self->{gffFeature}->{_gsf_tag_hash}->{Parent}->[0];
			if ( $parent =~ /path1$/){
				$self->{gffwriter}->write_feature($self->{currentGeneFeature});
				$self->{gffwriter}->write_feature($self->{gffFeature});
				$self->{validHitFound} = 1;

				# update the number of features written in output gff file
				$self->{totalWrittenFeatures}->{gene}++;
				$self->{totalWrittenFeatures}->{mrna}++;

			#	$multiLogger->debug(' !!!!!!! now writting  feature in gff file: ', Dumper $self->{gffFeature});

			# only print other path if all hits are selected from the cmd line
			} elsif ($self->{parameters}->{report} eq 'all'){
				$self->{gffwriter}->write_feature($self->{currentGeneFeature});
				$self->{gffwriter}->write_feature($self->{gffFeature});
				$self->{validHitFound} = 1;

				# update the number of features written in output gff file
				$self->{totalWrittenFeatures}->{gene}++;
				$self->{totalWrittenFeatures}->{mrna}++;
			#	$multiLogger->debug(' !!!!!!! now writting 2nd hit feature in gff file: ', Dumper $self->{gffFeature});

			}

		} else {
			#$multiLogger->debug(' hit with cov and id under thresholds: ', Dumper $self->{gffFeature});
			$self->{validHitFound} = 0;
		}
	}
}

sub addExonsCDSfromValidHits {
	my $self = shift;

	if ($self->{validHitFound} == 1 && $self->{gffFeature}->{_primary_tag} eq 'exon'){

		$self->{totalWrittenFeatures}->{exon}++;

	#	$multiLogger->debug(' feature contained in valid hit, now printing it into outfile: ', Dumper $self->{gffFeature});
		$self->{gffwriter}->write_feature($self->{gffFeature});

	} elsif ($self->{validHitFound} == 1 && $self->{gffFeature}->{_primary_tag} eq 'CDS'){

		$self->{totalWrittenFeatures}->{cds}++;

	#	$multiLogger->debug(' feature contained in valid hit, now printing it into outfile: ', Dumper $self->{gffFeature});
		$self->{gffwriter}->write_feature($self->{gffFeature});
	}

}

sub reportFinal {
	my $self = shift;

	my $totalWrittenFeatures = $self->{totalWrittenFeatures}->{gene};
	$totalWrittenFeatures +=$self->{totalWrittenFeatures}->{mrna};
	$totalWrittenFeatures +=$self->{totalWrittenFeatures}->{cds};
	$totalWrittenFeatures +=$self->{totalWrittenFeatures}->{exon};

	$multiLogger->info(" $totalWrittenFeatures Features written in final GFF ", $self->{files}->{output});
	$multiLogger->info(' Genes: ', $self->{totalWrittenFeatures}->{gene});
	$multiLogger->info(' mRNAs: ', $self->{totalWrittenFeatures}->{mrna});
	$multiLogger->info(' Exons: ', $self->{totalWrittenFeatures}->{exon});
	$multiLogger->info('   CDS: ', $self->{totalWrittenFeatures}->{cds});
}

sub setOptions {
	my $self = shift;

	$self->{files}->{input}=$Parameters{'input'};
	$self->{files}->{output}=defined $Parameters{'output'} ? $Parameters{'output'} : './parsed_gmap_results.gff';
	$self->{parameters}->{coverage}=defined $Parameters{'coverage'} ? $Parameters{'coverage'} : 100;
	$self->{parameters}->{identity}=defined $Parameters{'identity'} ? $Parameters{'identity'} : 100;
	$self->{parameters}->{report}=defined $Parameters{'report'} ? $Parameters{'report'} : 'best';
	$self->{parameters}->{help}=$help;
}

sub displayHelpMessage{
	# body...
	my $self = shift;

	$multiLogger->warn('##############################################################' . "\n");
	$multiLogger->warn('#           '.$script_name.' - Help (Version ' . $version . ')           #' . "\n");
	$multiLogger->warn('##############################################################' . "\n\n");

	$multiLogger->warn ('Usage example:' . "\n\n");
	$multiLogger->warn ("\t" . $script_name.' -gmap infile.gff -output results.gff -c 100 -I 100 -r best ' . "\n\n");

	$multiLogger->warn ('Mandatory parameters :' . "\n\n");

	$multiLogger->warn ("\t" . '-gmap/-g file => Path and name of the GMAP outputfile (with -f 2 format; mandatory).' . "\n\n");
	$multiLogger->warn ("\t" . '-output/-o file => Name for the output GFF file (mandatory).' . "\n\n");
	$multiLogger->warn ("\t" . '-coverage/-c int => coverage threshold for parsing (def=100%; mandatory).' . "\n\n");
	$multiLogger->warn ("\t" . '-identity/-I int => identity threshold for parsing (def=100%; mandatory).' . "\n\n");
	$multiLogger->warn ("\t" . '-report/-r all|best => report all or best path only (def=best; mandatory).' . "\n\n");
	$multiLogger->warn ("\n");

	exit (1);
}

sub checkParameters{

	my $self=shift;

	# Deal with input file
	if (!defined($self->{files}->{input})) {
		$multiLogger->error ('No input file defined !' . "\n\n");
		displayHelpMessage();
	} else {
		checkInputFile($self->{files}->{input}, 'GFF');
	}
}

sub checkInputFile {
	# Recovers parameters
	my ($file, $type) = @_;

	if (! -e $file || -z $file) {
		$multiLogger->fatal( 'Fatal error: The ' . $type . ' file specified in the command line does not exist or is empty ! Please check file name, path and content !!' . "\n");
		$multiLogger->fatal( 'Selected file: ' . basename($file) . "\n");
		$multiLogger->fatal( 'Corresponding directory: ' . dirname($file) . "\n\n");
		exit(1);
	}

	return 0;
}

sub createLogger {

	# Recovers parameters
	my $desiredLoggerLevel = defined $_[0] ? $_[0] : 'INFO';
	my $createFileLoggers = defined $_[1] ? $_[1] : 'no';

	if ($createFileLoggers !~ /[yes|no]/i) {
		print "The second parameters of the createLogger method can only be set to <yes> or <no> !\n";
		exit(1);
	}

	# Initializations
	my $programName = basename($0);

	# Create Logger and define basic level
	my $logger = get_logger('');
	$logger->level($desiredLoggerLevel);

	# Screen Appender
	my $appenderColors = { 'TRACE' => 'blue', 'DEBUG' => 'yellow', 'INFO' => '', 'WARN' => 'bold magenta', 'ERROR' => 'red', 'FATAL' => 'underline bold red' };
	my $std_appender = Log::Log4perl::Appender->new("Log::Log4perl::Appender::ScreenColoredLevels", 'name' => 'screen_log', 'stderr' => 0, 'color' => $appenderColors);
	$logger->add_appender($std_appender);

	# Layout for Screen Appender
	my $std_layout = Log::Log4perl::Layout::PatternLayout->new("%m%n");
	$std_appender->layout($std_layout);

	# Create file appenders if needed only
	if ($createFileLoggers eq 'yes') {
		# Basic file Appender
		my $basic_file_appender = Log::Log4perl::Appender->new(	"Log::Log4perl::Appender::File", name => 'basic_log', mode => 'write', filename  => $programName . '.log');
		$basic_file_appender->layout($std_layout);
		$basic_file_appender->threshold('INFO');
		$logger->add_appender($basic_file_appender);

		# Add appender for debug if needed
		if ($desiredLoggerLevel eq 'DEBUG') {

			my $debug_appender = Log::Log4perl::Appender->new("Log::Log4perl::Appender::File", name => 'debug_log', mode => 'write', filename  => $programName . '.debug');
			$logger->add_appender($debug_appender);

			my $debug_layout = Log::Log4perl::Layout::PatternLayout->new("%d %5p> %F{1}:%L %M - %m%n");
			$debug_appender->layout($debug_layout);

			$logger->level('DEBUG');
		}
	}


	return $logger;
}
